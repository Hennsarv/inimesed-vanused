﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace InimeneTest
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Inimene> inimesed = new List<Inimene>()
            {
                new Inimene("Henn", 62),
                new Inimene {Nimi = "Ants", Vanus = 40},
                new Inimene {Nimi = "Peeter", Vanus = 28},
                new Inimene {Nimi = "Jaak", Vanus = 70},
                new Inimene {Nimi = "Priit", Vanus = 69}

            };

            foreach (var x in inimesed) Console.WriteLine(x);

            if (inimesed.Count > 0)
            {

                Inimene vanim = inimesed[0];
                for (int i = 1; i < inimesed.Count; i++)
                {
                    if (inimesed[i].Vanus > vanim.Vanus) vanim = inimesed[i];
                }

                Console.WriteLine($"vanim on {vanim}");

                double sum = 0;
                foreach (var x in inimesed) sum += x.Vanus;
                Console.WriteLine($"Keskmine vanus on {sum/inimesed.Count}");
            }
            else
            {
                Console.WriteLine("inimesi ei ole");
            }

           
        }
    }

    class Inimene
    {
        private string _Nimi;
        private int _Vanus;

        public Inimene() { }

        public Inimene(string nimi, int vanus)
        {
            Nimi = nimi;
            Vanus = vanus;
        }


        public string Nimi
        {
            get => _Nimi;
            set => _Nimi = value.ToUpper()[0] + value.ToLower().Substring(1);
        }
        public int Vanus { get => _Vanus; set => _Vanus = value > 0 ? value : 0 ; }

        public override string ToString()
        {
            return $"Inimene {Nimi} vanusega {Vanus}";
        }
    }


}
